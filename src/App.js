import { Layout } from "antd";
import "./App.css";
import { CatsList } from "./cats/CatsList";

const { Content, Footer, Header } = Layout;

const headerStyle = {
  color: "#fff",
  textAlign: "center",
  height: 100,
};

const contentStyle = {
  textAlign: "center",
  minHeight: 120,
  lineHeight: "120px",
  margin: "50px",
};

const footerStyle = {
  textAlign: "center",
  color: "#fff",
  backgroundColor: "#001529",
};

function App() {
  return (
    <Layout>
      <Header style={headerStyle}>Привет, котики!</Header>
      <Content style={contentStyle}>
        <CatsList />
      </Content>
      <Footer style={footerStyle}>Созвездие ©2023</Footer>
    </Layout>
  );
}

export default App;
