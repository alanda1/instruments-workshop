import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders Привет, котики!", () => {
  render(<App />);
  const linkElement = screen.getByText(/Привет, котики!/i);
  expect(linkElement).toBeInTheDocument();
});
