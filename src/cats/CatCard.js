import { Card, Tag } from "antd";

const API_PREFIX = "https://cataas.com/cat";

export const CatCard = ({ cat }) => {
  return (
    <Card
      hoverable
      cover={
        <img
          alt="example"
          src={API_PREFIX + "/" + cat._id}
          style={{ objectFit: "cover", height: 240 }}
        />
      }
      style={{ marginBottom: 20 }}
    >
      {cat.tags.map((tag) => (
        <Tag key={tag}>{tag}</Tag>
      ))}
    </Card>
  );
};
