import { Col, Row } from "antd";
import { useEffect, useState } from "react";
import { CatCard } from "./CatCard";

const API_URL = "https://cataas.com/api/cats?limit=20";

export const CatsList = () => {
  const [cats, setCats] = useState([]);

  useEffect(() => {
    const getCats = async () => {
      const response = await fetch(API_URL);
      setCats(await response.json());
    };

    getCats();
  }, []);

  return (
    <Row gutter={16}>
      {cats.map((cat) => (
        <Col span={8} key={cat._id}>
          <CatCard cat={cat} />
        </Col>
      ))}
    </Row>
  );
};
